<?php
/* Smarty version 4.3.1, created on 2023-10-23 22:41:15
  from 'module:ps_featuredproductsviewstemplateshookps_featuredproducts.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '4.3.1',
  'unifunc' => 'content_6536a933dc55f5_38797633',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'fa6cc378d2942c8857b89d6bca728048c0caeedd' => 
    array (
      0 => 'module:ps_featuredproductsviewstemplateshookps_featuredproducts.tpl',
      1 => 1678742294,
      2 => 'module',
    ),
    'b78f7b5aa0c8fbd7a74d6e81a828429d9c6eb9f1' => 
    array (
      0 => '/var/www/html/BGPrestashop/themes/classic/templates/catalog/_partials/productlist.tpl',
      1 => 1678742294,
      2 => 'file',
    ),
    '03a1d9a860ae2f3e7d542f07270f3ba0a451726a' => 
    array (
      0 => '/var/www/html/BGPrestashop/themes/classic/templates/catalog/_partials/miniatures/product.tpl',
      1 => 1678742294,
      2 => 'file',
    ),
    '157660afc58f6363904748982598cd82850e8ae7' => 
    array (
      0 => '/var/www/html/BGPrestashop/themes/classic/templates/catalog/_partials/variant-links.tpl',
      1 => 1678742294,
      2 => 'file',
    ),
    'df2d5d5fe46c8fea5e7dd26d7b227693768fce32' => 
    array (
      0 => '/var/www/html/BGPrestashop/themes/classic/templates/catalog/_partials/product-flags.tpl',
      1 => 1678742294,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_6536a933dc55f5_38797633 (Smarty_Internal_Template $_smarty_tpl) {
?><section class="featured-products clearfix">
  <h2 class="h2 products-section-title text-uppercase">
    Popular Products
  </h2>
  

<div class="products row">
            
<div class="js-product product col-xs-12 col-sm-6 col-lg-4 col-xl-3">
  <article class="product-miniature js-product-miniature" data-id-product="1" data-id-product-attribute="1">
    <div class="thumbnail-container">
      <div class="thumbnail-top">
        
                      <a href="https://bgprestashop.bicsglobal.com/men/1-1-hummingbird-printed-t-shirt.html#/1-size-s/8-color-white" class="thumbnail product-thumbnail">
              <picture>
                                                <img
                  src="https://bgprestashop.bicsglobal.com/1-home_default/hummingbird-printed-t-shirt.jpg"
                  alt="Hummingbird printed t-shirt"
                  loading="lazy"
                  data-full-size-image-url="https://bgprestashop.bicsglobal.com/1-large_default/hummingbird-printed-t-shirt.jpg"
                  width="250"
                  height="250"
                />
              </picture>
            </a>
                  

        <div class="highlighted-informations">
          
            <a class="quick-view js-quick-view" href="#" data-link-action="quickview">
              <i class="material-icons search">&#xE8B6;</i> Quick view
            </a>
          

          
                          <div class="variant-links">
      <a href="https://bgprestashop.bicsglobal.com/men/1-3-hummingbird-printed-t-shirt.html#/2-size-m/8-color-white"
       class="color"
       title="White"
       aria-label="White"
       style="background-color: #ffffff"     ></a>
      <a href="https://bgprestashop.bicsglobal.com/men/1-2-hummingbird-printed-t-shirt.html#/1-size-s/11-color-black"
       class="color"
       title="Black"
       aria-label="Black"
       style="background-color: #434A54"     ></a>
    <span class="js-count count"></span>
</div>
                      
        </div>
      </div>

      <div class="product-description">
        
                      <h3 class="h3 product-title"><a href="https://bgprestashop.bicsglobal.com/men/1-1-hummingbird-printed-t-shirt.html#/1-size-s/8-color-white" content="https://bgprestashop.bicsglobal.com/men/1-1-hummingbird-printed-t-shirt.html#/1-size-s/8-color-white">Hummingbird printed t-shirt</a></h3>
                  

        
                      <div class="product-price-and-shipping">
                              

                <span class="regular-price" aria-label="Regular price">₹23.90</span>
                                  <span class="discount-percentage discount-product">-20%</span>
                              
              

              <span class="price" aria-label="Price">
                                                  ₹19.12
                              </span>

              

              
            </div>
                  

        
          
<div class="product-list-reviews" data-id="1" data-url="https://bgprestashop.bicsglobal.com/module/productcomments/CommentGrade">
  <div class="grade-stars small-stars"></div>
  <div class="comments-nb"></div>
</div>

        
      </div>

      
    <ul class="product-flags js-product-flags">
                    <li class="product-flag discount">-20%</li>
            </ul>

    </div>
  </article>
</div>

            
<div class="js-product product col-xs-12 col-sm-6 col-lg-4 col-xl-3">
  <article class="product-miniature js-product-miniature" data-id-product="2" data-id-product-attribute="9">
    <div class="thumbnail-container">
      <div class="thumbnail-top">
        
                      <a href="https://bgprestashop.bicsglobal.com/women/2-9-brown-bear-printed-sweater.html#/1-size-s" class="thumbnail product-thumbnail">
              <picture>
                                                <img
                  src="https://bgprestashop.bicsglobal.com/21-home_default/brown-bear-printed-sweater.jpg"
                  alt="Brown bear printed sweater"
                  loading="lazy"
                  data-full-size-image-url="https://bgprestashop.bicsglobal.com/21-large_default/brown-bear-printed-sweater.jpg"
                  width="250"
                  height="250"
                />
              </picture>
            </a>
                  

        <div class="highlighted-informations no-variants">
          
            <a class="quick-view js-quick-view" href="#" data-link-action="quickview">
              <i class="material-icons search">&#xE8B6;</i> Quick view
            </a>
          

          
                      
        </div>
      </div>

      <div class="product-description">
        
                      <h3 class="h3 product-title"><a href="https://bgprestashop.bicsglobal.com/women/2-9-brown-bear-printed-sweater.html#/1-size-s" content="https://bgprestashop.bicsglobal.com/women/2-9-brown-bear-printed-sweater.html#/1-size-s">Hummingbird printed sweater</a></h3>
                  

        
                      <div class="product-price-and-shipping">
                              

                <span class="regular-price" aria-label="Regular price">₹35.90</span>
                                  <span class="discount-percentage discount-product">-20%</span>
                              
              

              <span class="price" aria-label="Price">
                                                  ₹28.72
                              </span>

              

              
            </div>
                  

        
          
<div class="product-list-reviews" data-id="2" data-url="https://bgprestashop.bicsglobal.com/module/productcomments/CommentGrade">
  <div class="grade-stars small-stars"></div>
  <div class="comments-nb"></div>
</div>

        
      </div>

      
    <ul class="product-flags js-product-flags">
                    <li class="product-flag discount">-20%</li>
            </ul>

    </div>
  </article>
</div>

            
<div class="js-product product col-xs-12 col-sm-6 col-lg-4 col-xl-3">
  <article class="product-miniature js-product-miniature" data-id-product="3" data-id-product-attribute="13">
    <div class="thumbnail-container">
      <div class="thumbnail-top">
        
                      <a href="https://bgprestashop.bicsglobal.com/art/3-13-the-best-is-yet-to-come-framed-poster.html#/19-dimension-40x60cm" class="thumbnail product-thumbnail">
              <picture>
                                                <img
                  src="https://bgprestashop.bicsglobal.com/3-home_default/the-best-is-yet-to-come-framed-poster.jpg"
                  alt="The best is yet to come&#039; Framed poster"
                  loading="lazy"
                  data-full-size-image-url="https://bgprestashop.bicsglobal.com/3-large_default/the-best-is-yet-to-come-framed-poster.jpg"
                  width="250"
                  height="250"
                />
              </picture>
            </a>
                  

        <div class="highlighted-informations no-variants">
          
            <a class="quick-view js-quick-view" href="#" data-link-action="quickview">
              <i class="material-icons search">&#xE8B6;</i> Quick view
            </a>
          

          
                      
        </div>
      </div>

      <div class="product-description">
        
                      <h3 class="h3 product-title"><a href="https://bgprestashop.bicsglobal.com/art/3-13-the-best-is-yet-to-come-framed-poster.html#/19-dimension-40x60cm" content="https://bgprestashop.bicsglobal.com/art/3-13-the-best-is-yet-to-come-framed-poster.html#/19-dimension-40x60cm">The best is yet to come&#039;...</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="price" aria-label="Price">
                                                  ₹29.00
                              </span>

              

              
            </div>
                  

        
          
<div class="product-list-reviews" data-id="3" data-url="https://bgprestashop.bicsglobal.com/module/productcomments/CommentGrade">
  <div class="grade-stars small-stars"></div>
  <div class="comments-nb"></div>
</div>

        
      </div>

      
    <ul class="product-flags js-product-flags">
            </ul>

    </div>
  </article>
</div>

            
<div class="js-product product col-xs-12 col-sm-6 col-lg-4 col-xl-3">
  <article class="product-miniature js-product-miniature" data-id-product="4" data-id-product-attribute="16">
    <div class="thumbnail-container">
      <div class="thumbnail-top">
        
                      <a href="https://bgprestashop.bicsglobal.com/art/4-16-the-adventure-begins-framed-poster.html#/19-dimension-40x60cm" class="thumbnail product-thumbnail">
              <picture>
                                                <img
                  src="https://bgprestashop.bicsglobal.com/4-home_default/the-adventure-begins-framed-poster.jpg"
                  alt="The adventure begins Framed poster"
                  loading="lazy"
                  data-full-size-image-url="https://bgprestashop.bicsglobal.com/4-large_default/the-adventure-begins-framed-poster.jpg"
                  width="250"
                  height="250"
                />
              </picture>
            </a>
                  

        <div class="highlighted-informations no-variants">
          
            <a class="quick-view js-quick-view" href="#" data-link-action="quickview">
              <i class="material-icons search">&#xE8B6;</i> Quick view
            </a>
          

          
                      
        </div>
      </div>

      <div class="product-description">
        
                      <h3 class="h3 product-title"><a href="https://bgprestashop.bicsglobal.com/art/4-16-the-adventure-begins-framed-poster.html#/19-dimension-40x60cm" content="https://bgprestashop.bicsglobal.com/art/4-16-the-adventure-begins-framed-poster.html#/19-dimension-40x60cm">The adventure begins Framed...</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="price" aria-label="Price">
                                                  ₹29.00
                              </span>

              

              
            </div>
                  

        
          
<div class="product-list-reviews" data-id="4" data-url="https://bgprestashop.bicsglobal.com/module/productcomments/CommentGrade">
  <div class="grade-stars small-stars"></div>
  <div class="comments-nb"></div>
</div>

        
      </div>

      
    <ul class="product-flags js-product-flags">
            </ul>

    </div>
  </article>
</div>

            
<div class="js-product product col-xs-12 col-sm-6 col-lg-4 col-xl-3">
  <article class="product-miniature js-product-miniature" data-id-product="5" data-id-product-attribute="19">
    <div class="thumbnail-container">
      <div class="thumbnail-top">
        
                      <a href="https://bgprestashop.bicsglobal.com/art/5-19-today-is-a-good-day-framed-poster.html#/19-dimension-40x60cm" class="thumbnail product-thumbnail">
              <picture>
                                                <img
                  src="https://bgprestashop.bicsglobal.com/5-home_default/today-is-a-good-day-framed-poster.jpg"
                  alt="Today is a good day Framed poster"
                  loading="lazy"
                  data-full-size-image-url="https://bgprestashop.bicsglobal.com/5-large_default/today-is-a-good-day-framed-poster.jpg"
                  width="250"
                  height="250"
                />
              </picture>
            </a>
                  

        <div class="highlighted-informations no-variants">
          
            <a class="quick-view js-quick-view" href="#" data-link-action="quickview">
              <i class="material-icons search">&#xE8B6;</i> Quick view
            </a>
          

          
                      
        </div>
      </div>

      <div class="product-description">
        
                      <h3 class="h3 product-title"><a href="https://bgprestashop.bicsglobal.com/art/5-19-today-is-a-good-day-framed-poster.html#/19-dimension-40x60cm" content="https://bgprestashop.bicsglobal.com/art/5-19-today-is-a-good-day-framed-poster.html#/19-dimension-40x60cm">Today is a good day Framed...</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="price" aria-label="Price">
                                                  ₹29.00
                              </span>

              

              
            </div>
                  

        
          
<div class="product-list-reviews" data-id="5" data-url="https://bgprestashop.bicsglobal.com/module/productcomments/CommentGrade">
  <div class="grade-stars small-stars"></div>
  <div class="comments-nb"></div>
</div>

        
      </div>

      
    <ul class="product-flags js-product-flags">
            </ul>

    </div>
  </article>
</div>

            
<div class="js-product product col-xs-12 col-sm-6 col-lg-4 col-xl-3">
  <article class="product-miniature js-product-miniature" data-id-product="6" data-id-product-attribute="0">
    <div class="thumbnail-container">
      <div class="thumbnail-top">
        
                      <a href="https://bgprestashop.bicsglobal.com/home-accessories/6-mug-the-best-is-yet-to-come.html" class="thumbnail product-thumbnail">
              <picture>
                                                <img
                  src="https://bgprestashop.bicsglobal.com/6-home_default/mug-the-best-is-yet-to-come.jpg"
                  alt="Mug The best is yet to come"
                  loading="lazy"
                  data-full-size-image-url="https://bgprestashop.bicsglobal.com/6-large_default/mug-the-best-is-yet-to-come.jpg"
                  width="250"
                  height="250"
                />
              </picture>
            </a>
                  

        <div class="highlighted-informations no-variants">
          
            <a class="quick-view js-quick-view" href="#" data-link-action="quickview">
              <i class="material-icons search">&#xE8B6;</i> Quick view
            </a>
          

          
                      
        </div>
      </div>

      <div class="product-description">
        
                      <h3 class="h3 product-title"><a href="https://bgprestashop.bicsglobal.com/home-accessories/6-mug-the-best-is-yet-to-come.html" content="https://bgprestashop.bicsglobal.com/home-accessories/6-mug-the-best-is-yet-to-come.html">Mug The best is yet to come</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="price" aria-label="Price">
                                                  ₹11.90
                              </span>

              

              
            </div>
                  

        
          
<div class="product-list-reviews" data-id="6" data-url="https://bgprestashop.bicsglobal.com/module/productcomments/CommentGrade">
  <div class="grade-stars small-stars"></div>
  <div class="comments-nb"></div>
</div>

        
      </div>

      
    <ul class="product-flags js-product-flags">
            </ul>

    </div>
  </article>
</div>

            
<div class="js-product product col-xs-12 col-sm-6 col-lg-4 col-xl-3">
  <article class="product-miniature js-product-miniature" data-id-product="7" data-id-product-attribute="0">
    <div class="thumbnail-container">
      <div class="thumbnail-top">
        
                      <a href="https://bgprestashop.bicsglobal.com/home-accessories/7-mug-the-adventure-begins.html" class="thumbnail product-thumbnail">
              <picture>
                                                <img
                  src="https://bgprestashop.bicsglobal.com/7-home_default/mug-the-adventure-begins.jpg"
                  alt="Mug The adventure begins"
                  loading="lazy"
                  data-full-size-image-url="https://bgprestashop.bicsglobal.com/7-large_default/mug-the-adventure-begins.jpg"
                  width="250"
                  height="250"
                />
              </picture>
            </a>
                  

        <div class="highlighted-informations no-variants">
          
            <a class="quick-view js-quick-view" href="#" data-link-action="quickview">
              <i class="material-icons search">&#xE8B6;</i> Quick view
            </a>
          

          
                      
        </div>
      </div>

      <div class="product-description">
        
                      <h3 class="h3 product-title"><a href="https://bgprestashop.bicsglobal.com/home-accessories/7-mug-the-adventure-begins.html" content="https://bgprestashop.bicsglobal.com/home-accessories/7-mug-the-adventure-begins.html">Mug The adventure begins</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="price" aria-label="Price">
                                                  ₹11.90
                              </span>

              

              
            </div>
                  

        
          
<div class="product-list-reviews" data-id="7" data-url="https://bgprestashop.bicsglobal.com/module/productcomments/CommentGrade">
  <div class="grade-stars small-stars"></div>
  <div class="comments-nb"></div>
</div>

        
      </div>

      
    <ul class="product-flags js-product-flags">
            </ul>

    </div>
  </article>
</div>

            
<div class="js-product product col-xs-12 col-sm-6 col-lg-4 col-xl-3">
  <article class="product-miniature js-product-miniature" data-id-product="8" data-id-product-attribute="0">
    <div class="thumbnail-container">
      <div class="thumbnail-top">
        
                      <a href="https://bgprestashop.bicsglobal.com/home-accessories/8-mug-today-is-a-good-day.html" class="thumbnail product-thumbnail">
              <picture>
                                                <img
                  src="https://bgprestashop.bicsglobal.com/8-home_default/mug-today-is-a-good-day.jpg"
                  alt="Mug Today is a good day"
                  loading="lazy"
                  data-full-size-image-url="https://bgprestashop.bicsglobal.com/8-large_default/mug-today-is-a-good-day.jpg"
                  width="250"
                  height="250"
                />
              </picture>
            </a>
                  

        <div class="highlighted-informations no-variants">
          
            <a class="quick-view js-quick-view" href="#" data-link-action="quickview">
              <i class="material-icons search">&#xE8B6;</i> Quick view
            </a>
          

          
                      
        </div>
      </div>

      <div class="product-description">
        
                      <h3 class="h3 product-title"><a href="https://bgprestashop.bicsglobal.com/home-accessories/8-mug-today-is-a-good-day.html" content="https://bgprestashop.bicsglobal.com/home-accessories/8-mug-today-is-a-good-day.html">Mug Today is a good day</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="price" aria-label="Price">
                                                  ₹11.90
                              </span>

              

              
            </div>
                  

        
          
<div class="product-list-reviews" data-id="8" data-url="https://bgprestashop.bicsglobal.com/module/productcomments/CommentGrade">
  <div class="grade-stars small-stars"></div>
  <div class="comments-nb"></div>
</div>

        
      </div>

      
    <ul class="product-flags js-product-flags">
            </ul>

    </div>
  </article>
</div>

    </div>
  <a class="all-product-link float-xs-left float-md-right h4" href="https://bgprestashop.bicsglobal.com/2-home">
    All products<i class="material-icons">&#xE315;</i>
  </a>
</section>
<?php }
}
