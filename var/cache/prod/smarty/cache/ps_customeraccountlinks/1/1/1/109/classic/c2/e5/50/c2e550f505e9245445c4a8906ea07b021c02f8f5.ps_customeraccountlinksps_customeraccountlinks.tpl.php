<?php
/* Smarty version 4.3.1, created on 2023-10-17 18:51:51
  from 'module:ps_customeraccountlinksps_customeraccountlinks.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '4.3.1',
  'unifunc' => 'content_652e8a6f732541_59439255',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '42f9461127ce7396a601c2484841253ea5ba658f' => 
    array (
      0 => 'module:ps_customeraccountlinksps_customeraccountlinks.tpl',
      1 => 1678742294,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_652e8a6f732541_59439255 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
  'renderLogo' => 
  array (
    'compiled_filepath' => '/var/www/html/BGPrestashop/var/cache/prod/smarty/compile/classiclayouts_layout_full_width_tpl/8e/31/f8/8e31f8b9747edaafbe10bffd064ed97a3d6b3d08_2.file.helpers.tpl.php',
    'uid' => '8e31f8b9747edaafbe10bffd064ed97a3d6b3d08',
    'call_name' => 'smarty_template_function_renderLogo_1450790791651600fcbdf8e2_69011869',
  ),
));
?>
<div id="block_myaccount_infos" class="col-md-3 links wrapper">
  <p class="h3 myaccount-title hidden-sm-down">
    <a class="text-uppercase" href="https://bgprestashop.bicsglobal.com/my-account" rel="nofollow">
      Your account
    </a>
  </p>
  <div class="title clearfix hidden-md-up" data-target="#footer_account_list" data-toggle="collapse">
    <span class="h3">Your account</span>
    <span class="float-xs-right">
      <span class="navbar-toggler collapse-icons">
        <i class="material-icons add">&#xE313;</i>
        <i class="material-icons remove">&#xE316;</i>
      </span>
    </span>
  </div>
  <ul class="account-list collapse" id="footer_account_list">
            <li><a href="https://bgprestashop.bicsglobal.com/guest-tracking" title="Order tracking" rel="nofollow">Order tracking</a></li>
        <li><a href="https://bgprestashop.bicsglobal.com/my-account" title="Log in to your customer account" rel="nofollow">Sign in</a></li>
        <li><a href="https://bgprestashop.bicsglobal.com/registration" title="Create account" rel="nofollow">Create account</a></li>
        <li>
  <a href="//bgprestashop.bicsglobal.com/module/ps_emailalerts/account" title="My alerts">
    My alerts
  </a>
</li>

       
	</ul>
</div>
<?php }
}
