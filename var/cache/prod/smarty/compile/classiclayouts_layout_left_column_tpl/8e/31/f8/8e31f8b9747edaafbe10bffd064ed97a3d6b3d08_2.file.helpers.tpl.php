<?php
/* Smarty version 4.3.1, created on 2023-09-29 06:31:26
  from '/var/www/html/BGPrestashop/themes/classic/templates/_partials/helpers.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '4.3.1',
  'unifunc' => 'content_651621e6ef1c84_32121766',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8e31f8b9747edaafbe10bffd064ed97a3d6b3d08' => 
    array (
      0 => '/var/www/html/BGPrestashop/themes/classic/templates/_partials/helpers.tpl',
      1 => 1678742294,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_651621e6ef1c84_32121766 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
  'renderLogo' => 
  array (
    'compiled_filepath' => '/var/www/html/BGPrestashop/var/cache/prod/smarty/compile/classiclayouts_layout_left_column_tpl/8e/31/f8/8e31f8b9747edaafbe10bffd064ed97a3d6b3d08_2.file.helpers.tpl.php',
    'uid' => '8e31f8b9747edaafbe10bffd064ed97a3d6b3d08',
    'call_name' => 'smarty_template_function_renderLogo_1253484061651621e6eedec4_02430239',
  ),
));
?> 

<?php }
/* smarty_template_function_renderLogo_1253484061651621e6eedec4_02430239 */
if (!function_exists('smarty_template_function_renderLogo_1253484061651621e6eedec4_02430239')) {
function smarty_template_function_renderLogo_1253484061651621e6eedec4_02430239(Smarty_Internal_Template $_smarty_tpl,$params) {
foreach ($params as $key => $value) {
$_smarty_tpl->tpl_vars[$key] = new Smarty_Variable($value, $_smarty_tpl->isRenderingCache);
}
?>

  <a href="<?php echo htmlspecialchars((string) $_smarty_tpl->tpl_vars['urls']->value['pages']['index'], ENT_QUOTES, 'UTF-8');?>
">
    <img
      class="logo img-fluid"
      src="<?php echo htmlspecialchars((string) $_smarty_tpl->tpl_vars['shop']->value['logo_details']['src'], ENT_QUOTES, 'UTF-8');?>
"
      alt="<?php echo htmlspecialchars((string) $_smarty_tpl->tpl_vars['shop']->value['name'], ENT_QUOTES, 'UTF-8');?>
"
      width="<?php echo htmlspecialchars((string) $_smarty_tpl->tpl_vars['shop']->value['logo_details']['width'], ENT_QUOTES, 'UTF-8');?>
"
      height="<?php echo htmlspecialchars((string) $_smarty_tpl->tpl_vars['shop']->value['logo_details']['height'], ENT_QUOTES, 'UTF-8');?>
">
  </a>
<?php
}}
/*/ smarty_template_function_renderLogo_1253484061651621e6eedec4_02430239 */
}
