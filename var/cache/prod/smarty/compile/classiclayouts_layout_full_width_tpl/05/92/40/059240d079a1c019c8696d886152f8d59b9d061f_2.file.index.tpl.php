<?php
/* Smarty version 4.3.1, created on 2023-09-29 04:11:00
  from '/var/www/html/BGPrestashop/themes/classic/templates/index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '4.3.1',
  'unifunc' => 'content_651600fcbb9760_79040027',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '059240d079a1c019c8696d886152f8d59b9d061f' => 
    array (
      0 => '/var/www/html/BGPrestashop/themes/classic/templates/index.tpl',
      1 => 1678742294,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_651600fcbb9760_79040027 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_848726658651600fcbb72d2_04451982', 'page_content_container');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'page.tpl');
}
/* {block 'page_content_top'} */
class Block_1901505774651600fcbb7869_00661426 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'hook_home'} */
class Block_245039068651600fcbb8531_23914369 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php echo $_smarty_tpl->tpl_vars['HOOK_HOME']->value;?>

          <?php
}
}
/* {/block 'hook_home'} */
/* {block 'page_content'} */
class Block_2138694389651600fcbb8023_48826667 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_245039068651600fcbb8531_23914369', 'hook_home', $this->tplIndex);
?>

        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_848726658651600fcbb72d2_04451982 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_content_container' => 
  array (
    0 => 'Block_848726658651600fcbb72d2_04451982',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_1901505774651600fcbb7869_00661426',
  ),
  'page_content' => 
  array (
    0 => 'Block_2138694389651600fcbb8023_48826667',
  ),
  'hook_home' => 
  array (
    0 => 'Block_245039068651600fcbb8531_23914369',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-home">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1901505774651600fcbb7869_00661426', 'page_content_top', $this->tplIndex);
?>


        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2138694389651600fcbb8023_48826667', 'page_content', $this->tplIndex);
?>

      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
}
