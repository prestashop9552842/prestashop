<?php

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder889a2 = null;
    private $initializeree7a2 = null;
    private static $publicProperties7e823 = [
        
    ];
    public function getConnection()
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'getConnection', array(), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->getConnection();
    }
    public function getMetadataFactory()
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'getMetadataFactory', array(), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->getMetadataFactory();
    }
    public function getExpressionBuilder()
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'getExpressionBuilder', array(), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->getExpressionBuilder();
    }
    public function beginTransaction()
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'beginTransaction', array(), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->beginTransaction();
    }
    public function getCache()
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'getCache', array(), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->getCache();
    }
    public function transactional($func)
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'transactional', array('func' => $func), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->transactional($func);
    }
    public function wrapInTransaction(callable $func)
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'wrapInTransaction', array('func' => $func), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->wrapInTransaction($func);
    }
    public function commit()
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'commit', array(), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->commit();
    }
    public function rollback()
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'rollback', array(), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->rollback();
    }
    public function getClassMetadata($className)
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'getClassMetadata', array('className' => $className), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->getClassMetadata($className);
    }
    public function createQuery($dql = '')
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'createQuery', array('dql' => $dql), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->createQuery($dql);
    }
    public function createNamedQuery($name)
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'createNamedQuery', array('name' => $name), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->createNamedQuery($name);
    }
    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->createNativeQuery($sql, $rsm);
    }
    public function createNamedNativeQuery($name)
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->createNamedNativeQuery($name);
    }
    public function createQueryBuilder()
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'createQueryBuilder', array(), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->createQueryBuilder();
    }
    public function flush($entity = null)
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'flush', array('entity' => $entity), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->flush($entity);
    }
    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->find($className, $id, $lockMode, $lockVersion);
    }
    public function getReference($entityName, $id)
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->getReference($entityName, $id);
    }
    public function getPartialReference($entityName, $identifier)
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->getPartialReference($entityName, $identifier);
    }
    public function clear($entityName = null)
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'clear', array('entityName' => $entityName), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->clear($entityName);
    }
    public function close()
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'close', array(), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->close();
    }
    public function persist($entity)
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'persist', array('entity' => $entity), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->persist($entity);
    }
    public function remove($entity)
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'remove', array('entity' => $entity), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->remove($entity);
    }
    public function refresh($entity)
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'refresh', array('entity' => $entity), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->refresh($entity);
    }
    public function detach($entity)
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'detach', array('entity' => $entity), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->detach($entity);
    }
    public function merge($entity)
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'merge', array('entity' => $entity), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->merge($entity);
    }
    public function copy($entity, $deep = false)
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->copy($entity, $deep);
    }
    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->lock($entity, $lockMode, $lockVersion);
    }
    public function getRepository($entityName)
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'getRepository', array('entityName' => $entityName), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->getRepository($entityName);
    }
    public function contains($entity)
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'contains', array('entity' => $entity), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->contains($entity);
    }
    public function getEventManager()
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'getEventManager', array(), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->getEventManager();
    }
    public function getConfiguration()
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'getConfiguration', array(), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->getConfiguration();
    }
    public function isOpen()
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'isOpen', array(), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->isOpen();
    }
    public function getUnitOfWork()
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'getUnitOfWork', array(), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->getUnitOfWork();
    }
    public function getHydrator($hydrationMode)
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->getHydrator($hydrationMode);
    }
    public function newHydrator($hydrationMode)
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->newHydrator($hydrationMode);
    }
    public function getProxyFactory()
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'getProxyFactory', array(), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->getProxyFactory();
    }
    public function initializeObject($obj)
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'initializeObject', array('obj' => $obj), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->initializeObject($obj);
    }
    public function getFilters()
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'getFilters', array(), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->getFilters();
    }
    public function isFiltersStateClean()
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'isFiltersStateClean', array(), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->isFiltersStateClean();
    }
    public function hasFilters()
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'hasFilters', array(), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return $this->valueHolder889a2->hasFilters();
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);
        $instance->initializeree7a2 = $initializer;
        return $instance;
    }
    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;
        if (! $this->valueHolder889a2) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder889a2 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
        }
        $this->valueHolder889a2->__construct($conn, $config, $eventManager);
    }
    public function & __get($name)
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, '__get', ['name' => $name], $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        if (isset(self::$publicProperties7e823[$name])) {
            return $this->valueHolder889a2->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder889a2;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder889a2;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, '__set', array('name' => $name, 'value' => $value), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder889a2;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder889a2;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, '__isset', array('name' => $name), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder889a2;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder889a2;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, '__unset', array('name' => $name), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder889a2;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder889a2;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, '__clone', array(), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        $this->valueHolder889a2 = clone $this->valueHolder889a2;
    }
    public function __sleep()
    {
        $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, '__sleep', array(), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
        return array('valueHolder889a2');
    }
    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializeree7a2 = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializeree7a2;
    }
    public function initializeProxy() : bool
    {
        return $this->initializeree7a2 && ($this->initializeree7a2->__invoke($valueHolder889a2, $this, 'initializeProxy', array(), $this->initializeree7a2) || 1) && $this->valueHolder889a2 = $valueHolder889a2;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder889a2;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder889a2;
    }
}
